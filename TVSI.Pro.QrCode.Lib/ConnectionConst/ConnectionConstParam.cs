﻿using System.Configuration;

namespace TVSI.Pro.QrCode.Lib.ConnectionConst
{
    public class ConnectionConstParam
    {
        public static string ConnectionString_CRMDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CrmDB_Connection"].ConnectionString;
            }
        }    
        public static string ConnectionString_EMSDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ConnectionString;
            }
        }
    }
}
﻿namespace TVSI.Pro.QrCode.Lib.Model
{
    public class BranchIDModel
    {
        public string BranchID { get; set; }
        public string BranchName { get; set; }
    }
}
﻿using System;

namespace TVSI.Pro.QrCode.Lib.Model
{
    public class QrCodeModel
    {
        public long  QRCodeID { get; set; }
        public byte[] QRImage { get; set; }
        public string SaleID { get; set; }
        public string BranchID { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }

        public string LinkZalo
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    return "zalo.me/" + Phone + "";
                }
                return "";
            }
        }

        public int Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        
        
        
        
        
    }
}
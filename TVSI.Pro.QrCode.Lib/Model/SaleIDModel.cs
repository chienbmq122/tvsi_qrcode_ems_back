﻿namespace TVSI.Pro.QrCode.Lib.Model
{
    public class SaleIDModel
    {
        public string SaleID { get; set; }
        public string branchID { get; set; }
        public string FullName { get; set; }
        
        public byte[] QRImage { get; set; }
        public string Phone { get; set; }
        
    }
}
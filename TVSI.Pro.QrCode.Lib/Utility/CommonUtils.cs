﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TVSI.Pro.Investor.Lib.Utility
{
    public class CommonUtils
    {
        public static string ConvertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        
        
        public static string ParaphraseFileName(string fileName)
        {
            int lastIndex = fileName.LastIndexOf('.');
            string name = ConvertToUnSign(fileName.Substring(0, lastIndex)) + DateTime.Now.ToString("yyyyMMdd_HHmmss");
            string ext = fileName.Substring(lastIndex + 1);
            return name + "." + ext;
        }
    }
}

﻿namespace TVSI.Pro.QrCode.Lib.Constant
{
    public class AccountStatusConst
    {
        public const string CHO_XU_LY = "000"; // Chờ xử lý
        public const string DANG_XU_LY = "100"; // Đã kích hoạt (NVSS) = Đang xử lý
        public const string NO_HO_SO = "101"; // Đã kích hoạt nợ hồ sơ (NVSS)
        public const string KICH_HOAT = "102"; // Kích hoạt (KSV SS sau khi duyệt) = Kích hoạt
        public const string TU_CHOI_SS = "199"; // Từ chối (bời Nhân viên SS)
        public const string HOAN_THANH = "200"; // Hoàn thành (KSV duyệt)
        public const string TU_CHOI_KSV = "299"; // Từ chối (bởi KSV)
        public const string DA_XOA = "999"; // Đã xóa
    }
}
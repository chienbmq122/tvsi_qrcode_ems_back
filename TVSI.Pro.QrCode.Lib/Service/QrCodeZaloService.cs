﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using log4net;
using TVSI.Pro.QrCode.Lib.ConnectionConst;
using TVSI.Pro.QrCode.Lib.Model;

namespace TVSI.Pro.QrCode.Lib.Service
{
    public interface IQrCodeZaloService
    {
        List<QrCodeModel> GetQRCodeList(string saleID, string branchID, string fullname, string phone);
        SaleIDModel GetSaleInfo(string saleID);
        SaleIDModel GetSaleDetailInfo(int id);
        bool AddProfileSale(byte[] qrZalo, string saleID,string branchID,string phone,string fullName);
        bool CheckSaleQRInfo(string saleID);

        bool UpdateSaleInfo(int id, byte[] qrZalo,string saleID, string branchID,string phone,string fullName);

        List<BranchIDModel> GetBranchList();
        bool IsPhoneNumber(string number);
        bool DeleteQRInfo(int id);

    }
    public class QrCodeZaloService : IQrCodeZaloService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(QrCodeZaloService));

        public List<QrCodeModel> GetQRCodeList(string saleID, string branchID, string fullname, string phone)
        {
            try
            {
                
                var sql = "select * from QRCode where status = 1 {condition1} {condition2} {condition3} {condition4}";
                    sql = sql.Replace("{condition1}",!string.IsNullOrEmpty(saleID) ? "and SaleID = @saleID" : "");
                    sql = sql.Replace("{condition2}",!string.IsNullOrEmpty(branchID) ? "and BranchID = @branchID" : "");
                    sql = sql.Replace("{condition3}",!string.IsNullOrEmpty(fullname) ? "and FullName like @fullName" : "");
                    sql = sql.Replace("{condition4}",!string.IsNullOrEmpty(phone) ? "and Phone like @phone" : "");
                
                
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    return conn.Query<QrCodeModel>(sql, new
                    {
                        saleID = saleID,
                        branchID = branchID,
                        fullName = "%" +  fullname + "%",
                        phone = "%" + phone + "%"
                    }).OrderByDescending(x => x.QRCodeID).ToList();
                }
            }
            catch (Exception e)
            { 
                log.Info(string.Format("Loi Method GetQRCodeList {0}", e.Message));
                throw;
            }
        }

        public SaleIDModel GetSaleInfo(string saleID)
        {
            try
            {
                var sql =
                    "SELECT B.ho_va_ten as FullName, B.ma_nhan_vien_quan_ly as SaleID,C.ma_chi_nhanh as BranchID FROM TVSI_THONG_TIN_TRUY_CAP A INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id LEFT JOIN TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id WHERE b.ma_quan_ly = @saleID AND a.trang_thai = 1";
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_EMSDB))
                {
                    return conn.QueryFirstOrDefault<SaleIDModel>(sql, new
                    {
                        saleID = saleID.Substring(0,4)
                    });
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method GetSaleInfo {0}", e.Message));
                throw;
            }
        }

        public SaleIDModel GetSaleDetailInfo(int id)
        {
            try
            {
                var sql = "select * from QRCode where status = 1 and QRCodeID = @id";
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    return conn.QueryFirstOrDefault<SaleIDModel>(sql, new
                    {
                        id = id
                    });
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method GetSaleDetailInfo {0}", e.Message));
                throw;
            }
        }

        public bool AddProfileSale(byte[] qrZalo, string saleID,string branchID,string phone,string fullName)
        {
            try
            {
                var emsSession = HttpContext.Current.Session["user"];
                var sql = "insert into QRCode(QRImage,SaleID,BranchID,Phone,FullName,Status,CreatedBy,CreatedDate) values (@QRImage,@SaleID,@BranchID,@Phone,@FullName,1,@User,getdate())";
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    return conn.Execute(sql, new
                    {
                        QRImage = qrZalo,
                        SaleID = saleID,
                        BranchID = branchID,
                        Phone = phone,
                        User = emsSession,
                        FullName = fullName
                    }) > 0;
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method AddProfileSale {0}", e.Message));
                throw;
            }
        }

        public bool CheckSaleQRInfo(string saleID)
        {
            try
            {
                var sql = "select SaleID from QRCode where SaleID = @SaleID and Status = 1";
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    var data =  conn.QueryFirstOrDefault<string>(sql, new
                    {
                        SaleID = saleID.Substring(0,4)
                    });
                    return string.IsNullOrEmpty(data);
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method CheckSaleQRInfo {0}", e.Message));
                throw;
            }
        }

        public bool UpdateSaleInfo(int id,byte[] qrZalo, string saleID, string branchID, string phone, string fullName)
        {
            try
            {
                var emsSession = HttpContext.Current.Session["user"];

                var sql = "UPDATE QRCode set SaleID = @SaleID, BranchID = @BranchID, Phone = @Phone,FullName = @FullName,UpdatedBy = @CreateBy, UpdatedDate = getdate() {condition} where QRCodeID = @Id";
                sql = sql.Replace("{condition}", qrZalo.Length > 0 ? ", QRImage = @QRZalo" : "");
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    return conn.Execute(sql, new
                    {
                        QRZalo = qrZalo,
                        SaleID = saleID.Substring(0,4),
                        BranchID = branchID,
                        Phone = phone,
                        FullName = fullName,
                        CreateBy = emsSession,
                        Id = id
                    }) > 0;
                }
                
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method UpdateSaleInfo {0}", e.Message));
                throw;
            }
        }

        public List<BranchIDModel> GetBranchList()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_EMSDB))
                {
                    var sql = "SELECT ma_chi_nhanh BranchID,ten_chi_nhanh BranchName FROM TVSI_DANH_MUC_CHI_NHANH WHERE trang_thai = 1";
                    return conn.Query<BranchIDModel>(sql).ToList();
                }

            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method GetBranchList {0}", e.Message));
                throw;
            }
        }

        public bool IsPhoneNumber(string number)
        {
            int outValue;
            for (int i = 0; i < number.Length; i++)
            {
                if (!int.TryParse(number[i].ToString(), out outValue))
                    return false;
            }
            return true;
        }

        public bool DeleteQRInfo(int id)
        {
            try
            {
                var emsSession = HttpContext.Current.Session["user"];
                using (var conn = new SqlConnection(ConnectionConstParam.ConnectionString_CRMDB))
                {
                    var sql = "UPDATE QRCode SET Status = 99, UpdatedBy = @User,UpdatedDate = getdate() WHERE QRCodeID = @id ";
                    return conn.Execute(sql, new
                    {
                        id = id,
                        User = emsSession
                    }) > 0;
                }
            }
            catch (Exception e)
            {
                log.Info(string.Format("Loi Method DeleteQRInfo {0}", e.Message));
                throw;
            }
        }
    }
}
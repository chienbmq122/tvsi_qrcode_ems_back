﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TVSI.Pro.Investor.WebApp
{
    public class SessionHelper
    {
        public static void RecreateSession()
        {
            var vsdSession = HttpContext.Current.Session["user_vsd"];
            if (vsdSession != null)
                return;

            var emsSession = HttpContext.Current.Session["user"];
            if (emsSession != null)
            {
                var userName = emsSession.ToString();
                HttpContext.Current.Session["user_vsd"] = userName;
            }
        }

        public static string GetUserName()
        {
            var vsdSession = HttpContext.Current.Session["user_vsd"];
            if (vsdSession != null)
                return vsdSession.ToString();

            var emsSession = HttpContext.Current.Session["user"];
            if (emsSession != null)
                return emsSession.ToString();

            return "";
        }
    }
}
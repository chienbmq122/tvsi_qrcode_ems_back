﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace TVSI.Pro.QrCode.WebApp.PageView {
    
    
    public partial class QrCodeManager {
        
        /// <summary>
        /// XScript1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.XScript XScript1;
        
        /// <summary>
        /// form2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form2;
        
        /// <summary>
        /// Viewport1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Viewport Viewport1;
        
        /// <summary>
        /// Panel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Panel Panel1;
        
        /// <summary>
        /// txtSearchSaleID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtSearchSaleID;
        
        /// <summary>
        /// cboSearchBranchID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.ComboBox cboSearchBranchID;
        
        /// <summary>
        /// txtSearchFullName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtSearchFullName;
        
        /// <summary>
        /// txtSearchPhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtSearchPhone;
        
        /// <summary>
        /// btnSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button btnSearch;
        
        /// <summary>
        /// btnAdd7number control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button btnAdd7number;
        
        /// <summary>
        /// grdQRListService control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.GridPanel grdQRListService;
        
        /// <summary>
        /// storeQRListService control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Store storeQRListService;
        
        /// <summary>
        /// ColumnModel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.ColumnModel ColumnModel1;
        
        /// <summary>
        /// pageTBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.PagingToolbar pageTBar;
        
        /// <summary>
        /// Label1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Label Label1;
        
        /// <summary>
        /// ComboBox1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.ComboBox ComboBox1;
        
        /// <summary>
        /// RowSelectionModel1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.RowSelectionModel RowSelectionModel1;
        
        /// <summary>
        /// winServiceZaloDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Window winServiceZaloDetail;
        
        /// <summary>
        /// frmEditService control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FormPanel frmEditService;
        
        /// <summary>
        /// editID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden editID;
        
        /// <summary>
        /// txtEditSaleIDtxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditSaleIDtxt;
        
        /// <summary>
        /// cboEditBranchIDtxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.ComboBox cboEditBranchIDtxt;
        
        /// <summary>
        /// txtEditFullNametxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditFullNametxt;
        
        /// <summary>
        /// txtEditPhonetxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditPhonetxt;
        
        /// <summary>
        /// txtEditLinkZalotxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditLinkZalotxt;
        
        /// <summary>
        /// txtEditImagetxt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Image txtEditImagetxt;
        
        /// <summary>
        /// txtEditUploadQRZalo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FileUploadField txtEditUploadQRZalo;
        
        /// <summary>
        /// winUserQRDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Window winUserQRDetail;
        
        /// <summary>
        /// frmEditUser control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FormPanel frmEditUser;
        
        /// <summary>
        /// hidID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hidID;
        
        /// <summary>
        /// txtEditSaleID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditSaleID;
        
        /// <summary>
        /// txtEditBranchID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditBranchID;
        
        /// <summary>
        /// txtEditFullName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditFullName;
        
        /// <summary>
        /// txtEditPhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtEditPhone;
        
        /// <summary>
        /// txtEditImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Image txtEditImage;
        
        /// <summary>
        /// winDeleteQRCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Window winDeleteQRCode;
        
        /// <summary>
        /// formDelQR control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FormPanel formDelQR;
        
        /// <summary>
        /// hidDelID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Hidden hidDelID;
        
        /// <summary>
        /// txtDelSaleID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtDelSaleID;
        
        /// <summary>
        /// txtDelBranchID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtDelBranchID;
        
        /// <summary>
        /// txtDelFullName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtDelFullName;
        
        /// <summary>
        /// txtDelPhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtDelPhone;
        
        /// <summary>
        /// txtDelImage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Image txtDelImage;
        
        /// <summary>
        /// winPopupAdd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Window winPopupAdd;
        
        /// <summary>
        /// frmAddUser7number control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FormPanel frmAddUser7number;
        
        /// <summary>
        /// txtAddSaleID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtAddSaleID;
        
        /// <summary>
        /// txtButtonGetSaleInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button txtButtonGetSaleInfo;
        
        /// <summary>
        /// txtAddBranchID control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtAddBranchID;
        
        /// <summary>
        /// txtAddFullName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtAddFullName;
        
        /// <summary>
        /// txtAddPhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.TextField txtAddPhone;
        
        /// <summary>
        /// UploadQRZaloFiled control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.FileUploadField UploadQRZaloFiled;
        
        /// <summary>
        /// btnConfAddUser7number control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.Button btnConfAddUser7number;
        
        /// <summary>
        /// KeyMap1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.KeyMap KeyMap1;
        
        /// <summary>
        /// KeyMap2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify, move the field declaration from the designer file to a code-behind file.
        /// </remarks>
        protected global::Ext.Net.KeyMap KeyMap2;
    }
}

﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using Ext.Net;
using TVSI.Pro.QrCode.Lib.Service;
using TVSI.Utility;

namespace TVSI.Pro.QrCode.WebApp.PageView
{
    public partial class QrCodeManager : Page
    {
        private IQrCodeZaloService _qrCodeZaloService;

        public QrCodeManager()
        {
            _qrCodeZaloService = new QrCodeZaloService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            /*/*test session ko đẩy lên thật#1#
            Session["user"] = "chientm";
            /*tessss#1#*/
            var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }
            var branchList = _qrCodeZaloService.GetBranchList();
            for (int i = 0; i < branchList.Count; i++)
            {
                cboSearchBranchID.Items.Add(new ListItem(branchList[i].BranchID,branchList[i].BranchID));
                cboEditBranchIDtxt.Items.Add(new ListItem(branchList[i].BranchID,branchList[i].BranchID));
            }

            if (!IsPostBack)
            {
                pInitControls(Controls);
                BindData();
            }
        }
        
        public static void pInitControls(ControlCollection aAllControls)
        {

            foreach (Control c in aAllControls)
            {
                if (c is TextField)
                {
                    ((TextField)c).Text ="" ;  
                }
                pInitControls(c.Controls);
            }

        }

        protected void Refresh_Data(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(QrCodeManager), string.Format("Loi method FreshData {0}",ex.Message));
                throw;
            }
        }

        public void BindData()
        {
            var userUpdate = Session["user"] != null ? Session["user"].ToString() : "";
            if (string.IsNullOrEmpty(userUpdate))
            {
                Response.Redirect("http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx");
                X.Js.Call("logout");
                return;
            }
            var keySaleID = txtSearchSaleID.Text;
            var keyBranchID = cboSearchBranchID.SelectedItem.Value;
            var keyFullName = txtSearchFullName.Text;
            var keyPhone = txtSearchPhone.Text;
            pInitControls(Controls);

          var data = _qrCodeZaloService.GetQRCodeList(keySaleID,keyBranchID,keyFullName,keyPhone);
          grdQRListService.GetStore().DataSource = data;
          grdQRListService.GetStore().DataBind();
        }
        public void btnAddUser7number_Click(object sender, EventArgs e)
        {
            winPopupAdd.Show();

            /*dtxAddNgay_ket_thuc7number.ReadOnly = true;
            txtAddSo_tai_khoan7number.Text = "";
            txtAddTen_khach_hang7number.Text = "";
            var goihientai = cboAddGDV_hien_tai7number;
            var goimoi = cboAddGDV_moi7number.SelectedIndex = 1;
            dtxAddNgay_dang_ky7number.Value = DateTime.Now;
            dtxAddNgay_ket_thuc7number.Value = DateTime.MaxValue;*/
        }

        public void GetSaleInfo(object sender, EventArgs e)
        {
            try
            {
                var saleID = txtAddSaleID.Text;
                if (!string.IsNullOrEmpty(saleID))
                {
                    var data = _qrCodeZaloService.GetSaleInfo(saleID);
                    if (data != null)
                    {
                        txtAddBranchID.Text = data.branchID;
                        txtAddFullName.Text = data.FullName;
                    }
                    else
                    {
                        txtAddBranchID.Text = "";
                        txtAddFullName.Text = "";
                        ExtNet.Msg.Show(new MessageBoxConfig
                        {
                            Title = "Thông báo",
                            Buttons = MessageBox.Button.OK,
                            Icon = MessageBox.Icon.WARNING,
                            Message = "Không tìm thấy nhân viên môi giới!!!!"
                        }); 
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(QrCodeManager), string.Format("Loi method GetSaleInfo {0}",ex.Message));
                throw;
            }
        }

        public void AddProfileQRZalo(object sender, EventArgs e)
        {
            try
            {
                var saleID = txtAddSaleID.Text;
                var branchID = txtAddBranchID.Text;
                var fullName = txtAddFullName.Text;
                var phone = txtAddPhone.Text;
                HttpPostedFile file = UploadQRZaloFiled.PostedFile;
                var fileSize = file.ContentLength;
               var validSale = _qrCodeZaloService.CheckSaleQRInfo(saleID);
               var isPhone = _qrCodeZaloService.IsPhoneNumber(phone);
               if (!isPhone)
               {
                   ExtNet.Msg.Show(new MessageBoxConfig
                   {
                       Title = "Thông báo",
                       Buttons = MessageBox.Button.OK,
                       Icon = MessageBox.Icon.WARNING,
                       Message = "Không phải số điện thoại, vui lòng nhập lại!!!"
                   }); 
                   return;
               }
               if (!validSale)
               {
                   ExtNet.Msg.Show(new MessageBoxConfig
                   {
                       Title = "Thông báo",
                       Buttons = MessageBox.Button.OK,
                       Icon = MessageBox.Icon.WARNING,
                       Message = "Thông tin SaleID đã tồn tại"
                   }); 
                   return;
               }
               if (file.ContentLength <= 0)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Vui lòng tải lên file!"
                    }); 
                    return;

                }
                if (string.IsNullOrEmpty(saleID))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Mã SaleID ko được trống!!!"
                    }); 
                    return;

                }  
                if (string.IsNullOrEmpty(branchID))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Mã chi nhánh ko được trống!!!"
                    });  
                    return;

                }    
                if (string.IsNullOrEmpty(fullName))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Họ và tên ko được trống!!!"
                    });  
                    return;

                }     
                if (string.IsNullOrEmpty(phone))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "số điện thoại ko được trống!!!"
                    });  
                    return;

                }
                var uploadFile = new byte[file.InputStream.Length];
                if (fileSize / 1048576.0 <= 3)
                {
                    file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                }
                var data =  _qrCodeZaloService.AddProfileSale(uploadFile, saleID, branchID, phone,fullName);

                if (!data)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Lưu không thành công, vui lòng liên hệ IT!!!"
                    });    
                    return;
                }
                BindData();
                winPopupAdd.Hide();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(QrCodeManager), string.Format("Loi method AddProfileQRZalo {0}",ex.Message));
                throw;
            }
        }
        
        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewUserQRDetail(int id)
        {
            var data = _qrCodeZaloService.GetSaleDetailInfo(id);
            if (data != null)
            {
                txtEditSaleID.Text = data.SaleID;
                txtEditBranchID.Text = data.branchID;
                txtEditFullName.Text = data.FullName;
                txtEditPhone.Text = data.Phone;
                if (data.QRImage != null)
                {
                    var base64 = Convert.ToBase64String(data.QRImage);
                    txtEditImage.ImageUrl = "data:image/jpg;base64," + base64;
                }
            }
            winUserQRDetail.Show();
            hidID.Value = id;
        }
        
        [DirectMethod(Namespace = "USERMETHOD")]
        public void ViewServiceQRDetail(int id)
        {
            editID.Value = id;
            var data = _qrCodeZaloService.GetSaleDetailInfo(id);
            if (data != null)
            {
                txtEditSaleIDtxt.Text = data.SaleID;
                txtEditFullNametxt.Text = data.FullName;
                txtEditPhonetxt.Text = data.Phone;
                cboEditBranchIDtxt.SelectedItem.Value = data.branchID;
                txtEditLinkZalotxt.Text = "zalo.me/" + data.Phone + "";
                if (data.QRImage != null)
                {
                    var base64 = Convert.ToBase64String(data.QRImage);
                    txtEditImagetxt.ImageUrl = "data:image/jpg;base64," + base64;
                }
            }
            winServiceZaloDetail.Show();
            

        }
        
        [DirectMethod(Namespace = "USERMETHOD")]
        public void DeleteUserQRInfo(int id)
        {
            hidDelID.Value = id;
            var data = _qrCodeZaloService.GetSaleDetailInfo(id);
            if (data != null)
            {
                txtDelSaleID.Text = data.SaleID;
                txtDelBranchID.Text = data.branchID;
                txtDelFullName.Text = data.FullName;
                txtDelPhone.Text = data.Phone;
                if (data.QRImage != null)
                {
                    var base64 = Convert.ToBase64String(data.QRImage);
                    txtDelImage.ImageUrl = "data:image/jpg;base64," + base64;
                }
            }
            winDeleteQRCode.Show();
        }
        public void btnDeleteUser_Click(object sender, EventArgs e)
        {

            var id = Convert.ToInt32(hidDelID.Value);
            var changed = _qrCodeZaloService.DeleteQRInfo(id);
            if (changed)
            {
                winDeleteQRCode.Hide();
                BindData();
            }
        }
        public void btnViewUser_Click(object sender, EventArgs e)
        {
            winUserQRDetail.Hide();
        }

        public void EditQRZaloInfo(object sender, EventArgs e)
        {
            try
            {
                var idtxt = editID.Text;
                var id = int.Parse(idtxt);
                var saleID = txtEditSaleIDtxt.Text;
                var branchID = cboEditBranchIDtxt.SelectedItem.Value;
                var fullName = txtEditFullNametxt.Text;
                var phone = txtEditPhonetxt.Text;
                HttpPostedFile file = txtEditUploadQRZalo.PostedFile;
                var uploadFile = new byte[] { };
                var isPhone = _qrCodeZaloService.IsPhoneNumber(phone);
                if (!isPhone)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Không phải số điện thoại, vui lòng nhập lại!!!"
                    }); 
                    return;
                }
                if (string.IsNullOrEmpty(saleID))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Mã SaleID ko được trống!!!"
                    });  
                    return;
                }  
                if (string.IsNullOrEmpty(branchID))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Mã chi nhánh ko được trống!!!"
                    });  
                    return;
                }    
                if (string.IsNullOrEmpty(fullName))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "Họ và tên ko được trống!!!"
                    });  
                    return;
                }     
                if (string.IsNullOrEmpty(phone))
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.WARNING,
                        Message = "số điện thoại ko được trống!!!"
                    });
                    return;
                }
                if (file.ContentLength > 0)
                {
                    var fileSize = file.ContentLength;
                    uploadFile = new byte[file.InputStream.Length];
                    if (fileSize / 1048576.0 <= 3)
                    {
                        file.InputStream.Read(uploadFile, 0, uploadFile.Length);
                    } 
                }
                var data = _qrCodeZaloService.UpdateSaleInfo(id,uploadFile, saleID, branchID, phone, fullName);
                if (!data)
                {
                    ExtNet.Msg.Show(new MessageBoxConfig
                    {
                        Title = "Thông báo",
                        Buttons = MessageBox.Button.OK,
                        Icon = MessageBox.Icon.ERROR,
                        Message = "Lưu không thành công, vui lòng liên hệ IT!!!"
                    });  
                    return;
                }
                BindData();
                winServiceZaloDetail.Hide();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(QrCodeManager), string.Format("Loi method EditQRZaloInfo {0}", ex.Message));
                throw;
            }
        }
    }
}
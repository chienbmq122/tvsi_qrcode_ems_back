﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="TVSI.Pro.Investor.WebApp.Page.Register" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner { font-weight: bold !important; }   
        
        .cbStates-list {
            font: 11px tahoma, arial, helvetica, sans-serif;
            width: 298px;
        }

        .cbStates-list th { font-weight: bold; }

        .cbStates-list td, .cbStates-list th { padding: 3px; }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
            
            function grdRequestListCommand(command, data) {
                if (command == "Edit") {
                    ViewRequest(data);
                }
                
                if (command == "Log") {
                    ViewActionLog(data);
                }
            }
            
            cboTransactionFeeOnSelect = function(combo, record, index) {
                this.triggers[0].show();
            };
            
            cboCustomerBankOnSelect = function(combo, record, index) {
                this.triggers[0].show(); 
                #{hidBankCode}.setValue(record.data.bank_code);
                #{hidBankCheqCode}.setValue(record.data.bank_cheq_code);
                #{hidBranchCode}.setValue(record.data.branch_code);
                #{frmUpdateRequest_txtBankAccount}.setValue(record.data.bank_account);
                #{frmUpdateRequest_txtBankBeneficiary}.setValue(record.data.bank_beneficiary);
                #{frmUpdateRequest_txtBankName}.setValue(record.data.bank_name);
                #{frmUpdateRequest_txtBranchName}.setValue(record.data.branch_name);
            };
            
            cboBankListOnSelect = function(combo, record, index) {
                this.triggers[0].show(); 
                #{hidBankCode}.setValue(record.data.bank_code);
                #{hidBankCheqCode}.setValue(record.data.bank_cheq_code);
                #{hidBranchCode}.setValue(record.data.branch_code);
                #{frmUpdateRequest_txtBankName}.setValue(record.data.bank_name);
                #{frmUpdateRequest_txtBranchName}.setValue(record.data.branch_name);
            };
            
            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';
            
            var renderRequestWorkflow = function (value) {
                if (value == "1")
                    return String.format(template, "#fac776", "BMS");
                if (value == "2")
                    return String.format(template, "#96cfb9", "BB, BS");
                
                return value;
            };
            
            var renderRequestStatus = function (value) {
                if (value == "10")
                    return String.format(template, "#ffffff", "Chờ xử lý");
                if (value == "20")
                    return String.format(template, "#f2d295", "Đã mua TT");
                if (value == "30")
                    return String.format(template, "#fac776", "Đã bán TT");
                if (value == "40")
                    return String.format(template, "#fac776", "Đã chuyển tiền");
                if (value == "50")
                    return String.format(template, "#fac776", "Đã hoàn phí");
                if (value == "60")
                    return String.format(template, "#fac776", "Đã chuyển phí");
                if (value == "100")
                    return String.format(template, "#96cfb9", "Hoàn thành");
                if (value == "-10")
                    return String.format(template, "#a11d10", "Đã hủy");
                if (value == "-20")
                    return String.format(template, "#858994", "Từ chối");
                
                return value;
            };
            
            var checkSelectRow = function(sm) {
                if (sm.getCount() > 0)
                    return true;

                return false;
            };
            
            var checkCancelledRow = function(sm) {
                if (sm.getCount() == 1)
                    return true;

                return false;
            };
            
            var columnAutoResize = function (grid) {
                var view = grid.getView(),
                    store = grid.getStore(),
                    colModel = grid.getColumnModel(),
                    columns = colModel.config,
                    maxAutoWidth = 250, //0 to disable
                    cell,
                    value,
                    width = 0;
            
                Ext.each(columns, function (column, colIdx) {
                    // Data Width
                    var colWidth = width;
                    store.each(function (record, rowIdx) {
                        cell = view.getCell(rowIdx, colIdx);
                        value = record.get(column.dataIndex);
                        colWidth = Math.max(colWidth, Ext.util.TextMetrics.measure(cell, value).width);
                    });
            
                    if (!column.isRowNumberer) {
                        // Header Width
                        header = view.getHeaderCell(colIdx);
                        headerWidth = Ext.util.TextMetrics.measure(header, column.header).width;
                    }
                    else {
                        // Calc width using total rows
                        lengthWidth = store.getTotalCount().toString().length;
                        headerWidth = (lengthWidth * 10) + (lengthWidth > 1 ? 0 : 10);
                    }
            
                    // Choose the biggest width
                    if (colWidth < headerWidth || colWidth == 0) {
                        colWidth = headerWidth;
                    }
            
                    // Max Length
                    if (colWidth > maxAutoWidth && maxAutoWidth > 0) {
                        colWidth = maxAutoWidth;
                    }
            
                    //Add space to avoid ...
                    if (!column.isRowNumberer) {
                        colWidth += 20
                    }
            
                    colModel.setColumnWidth(colIdx, colWidth);
                });
            };
        </script>
    </ext:XScript>
    <script type="text/javascript">
        function IsNullOrWhiteSpace(str){
            return str === null || str.match(/^\s*$/) !== null;
        }
        
        var ViewRequest = function (selectedRequest) {
            REQUESTLIST.ViewRequest(selectedRequest);
        }
        
        var ViewActionLog = function (selectedRequest) {
            REQUESTLIST.ViewActionLog(selectedRequest);
        }
        
        /*var rejectionHandler = function (buttonId, reason) {
            if (buttonId == 'ok') {
                if (!IsNullOrWhiteSpace(reason))
                {
                    REQUESTLIST.RejectRequest(reason);
                } 
                else 
                {
                    Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập lý do từ chối', buttons: Ext.Msg.OK}); 
                    return false;
                }
            }
            else 
            {
                REQUESTLIST.ClearSelectedRows();      
            }
        };*/
    </script>
</head>
<body>
<form id="form1" runat="server">
<ext:ResourceManager runat="server"/>
<ext:Viewport runat="server" layout="BorderLayout">
<Items>
<ext:FormPanel Margins="0 0 0 0" runat="server" Region="North" ButtonAlign="Center" ID="frmSearchRequest" Height="100">
    <Items>
        <ext:Container runat="server" Layout="Column" Height="100">
            <Items>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".1">
                    <Items>
                        <ext:Hidden runat="server" ID="hidRequestId"/>
                        <ext:Hidden runat="server" ID="hidRequestStatus"/>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:TextField ID="frmSearchRequest_txtCustomerCode" runat="server" FieldLabel="Số tài khoản" Width="200" LabelStyle="font-weight:bold;"
                                       MsgTarget="Side" AnchorHorizontal="50%"/>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:DateField runat="server" ID="frmSearchRequest_dtxFromDate" FieldLabel="Từ ngày" Format="dd/MM/yyyy" MaskRe="[0-9\/]" LabelStyle="font-weight:bold;" AnchorHorizontal="50%">
                            <DirectEvents>
                                <Change onEvent="frmSearchRequest_dtxFromDate_Change"></Change>
                            </DirectEvents>
                        </ext:DateField>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:DateField runat="server" ID="frmSearchRequest_dtxToDate" FieldLabel="Đến ngày" Format="dd/MM/yyyy" MaskRe="[0-9\/]" LabelStyle="font-weight:bold;" AnchorHorizontal="50%">
                            <DirectEvents>
                                <Change onEvent="frmSearchRequest_dtxToDate_Change"></Change>
                            </DirectEvents>
                        </ext:DateField>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:ComboBox runat="server" ID="frmSearchRequest_cboRequestStatus" FieldLabel="Trạng thái" Editable="false" SelectedIndex="0" Width="120" LabelStyle="font-weight:bold;">
                            <Items>
                                <ext:ListItem Text="--- ALL ---" Value="-1"/>
                                <ext:ListItem Text="Chờ xử lý" Value="10"/>
                                <ext:ListItem Text="Đã hủy" Value="-10"/>
                                <ext:ListItem Text="Từ chối" Value="-20"/>
                                <ext:ListItem Text="Đã mua TT" Value="20"/>
                                <ext:ListItem Text="Đã bán TT" Value="30"/>
                                <ext:ListItem Text="Đã chuyển tiền" Value="40"/>
                                <ext:ListItem Text="Đã hoàn phí" Value="50"/>
                                <ext:ListItem Text="Đã chuyển phí" Value="60"/>
                                <ext:ListItem Text="Hoàn thành" Value="100"/>
                            </Items>
                            <SelectedItem Value="-1"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{storeRequestList}.reload()"></Select>
                            </Listeners>
                            <DirectEvents>
                                <Select onEvent="frmSearchRequest_cboRequestStatus_Select"></Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".1">
                    <Items>
                        <ext:ComboBox runat="server" ID="frmSearchRequest_cboRequestWorkflow" FieldLabel="Luồng xử lý" Editable="false" SelectedIndex="0" Width="120" LabelStyle="font-weight:bold;">
                            <Items>
                                <ext:ListItem Text="--- ALL ---" Value="-1"/>
                                <ext:ListItem Text="Vận hành trái phiếu (BMS)" Value="1"/>
                                <ext:ListItem Text="Kinh doanh trái phiếu (BB, BS)" Value="2"/>
                            </Items>
                            <SelectedItem Value="-1"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{storeRequestList}.reload()"></Select>
                            </Listeners>
                            <DirectEvents>
                                <Select onEvent="frmSearchRequest_cboRequestWorkflow_Select"></Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </Items>
                </ext:Container>
            </Items>
        </ext:Container>
    </Items>
    <Buttons>
        <ext:Button runat="server" Text="Tìm kiếm" ID="frmSearchRequest_btnSearch" Icon="Find" CausesValidation="true">
            <DirectEvents>
                <Click OnEvent="frmSearchRequest_btnSearch_Click"></Click>
            </DirectEvents>
            <Listeners>
                <Click Handler="#{storeRequestList}.reload()"></Click>
            </Listeners>
        </ext:Button>
        <ext:Button runat="server" Text="Reset" ID="frmSearchRequest_btnReset" Icon="Stop">
            <Listeners>
                <Click Handler="#{frmSearchRequest}.reset()"></Click>
            </Listeners>
            <DirectEvents>
                <Click OnEvent="frmSearchRequest_btnReset_Click"></Click>
            </DirectEvents>
        </ext:Button>
        <ext:Button runat="server" Text="Đăng ký" ID="frmSearchRequest_btnRegister" Icon="PageAdd">
            <DirectEvents>
                <Click OnEvent="frmSearchRequest_btnRegister_Click"></Click>
            </DirectEvents>
        </ext:Button>
    </Buttons>
</ext:FormPanel>
<ext:GridPanel ID="grdRequestList" runat="server" Margins="0 0 0 0" Region="Center"
               ColumnLines="True" StripeRows="True" ButtonAlign="Center">
    <Store>
        <ext:Store ID="storeRequestList" runat="server" OnRefreshData="storeRequestList_Refresh">
            <Reader>
                <ext:JsonReader IDProperty="request_id">
                    <Fields>
                        <ext:RecordField Name="request_id"/>
                        <ext:RecordField Name="customer_code"/>
                        <ext:RecordField Name="customer_source"/>
                        <ext:RecordField Name="workflow_id"/>
                        <ext:RecordField Name="customer_name"/>
                        <ext:RecordField Name="customer_birthday"/>
                        <ext:RecordField Name="identity_number"/>
                        <ext:RecordField Name="issue_place"/>
                        <ext:RecordField Name="issue_date"/>
                        <ext:RecordField Name="customer_type"/>
                        <ext:RecordField Name="customer_nationality"/>
                        <ext:RecordField Name="customer_address"/>
                        <ext:RecordField Name="bank_account"/>
                        <ext:RecordField Name="bank_beneficiary"/>
                        <ext:RecordField Name="bank_code"/>
                        <ext:RecordField Name="bank_cheq_code"/>
                        <ext:RecordField Name="branch_code"/>
                        <ext:RecordField Name="bank_name"/>
                        <ext:RecordField Name="branch_name"/>
                        <ext:RecordField Name="request_note"/>
                        <ext:RecordField Name="request_status"/>
                        <ext:RecordField Name="rejection_reason"/>
                        <ext:RecordField Name="created_date"/>
                        <ext:RecordField Name="created_by"/>
                        <ext:RecordField Name="updated_by"/>
                        <ext:RecordField Name="updated_date"/>
                    </Fields>
                </ext:JsonReader>
            </Reader>
        </ext:Store>
    </Store>
    <ColumnModel runat="server">
        <Columns>
            <ext:Column Header="STT" Fixed="true" Sortable="false" ColumnID="stt" MenuDisabled="true" Width="40">
                <Renderer Fn="function (v, p, record, rowIndex) {return rowIndex + 1}"></Renderer>
            </ext:Column>
            <ext:CommandColumn Width="40">
                <Commands>
                    <ext:GridCommand Icon="ApplicationEdit" CommandName="Edit">
                        <ToolTip Text="View/Edit"></ToolTip>
                    </ext:GridCommand>
                </Commands>
            </ext:CommandColumn>
            <ext:CommandColumn Width="40">
                <Commands>
                    <ext:GridCommand Icon="BookOpenMark" CommandName="Log">
                        <ToolTip Text="Log"></ToolTip>
                    </ext:GridCommand>
                </Commands>
            </ext:CommandColumn>
            <ext:Column DataIndex="request_id" Header="Mã định danh" Width="100"/>
            <ext:DateColumn DataIndex="created_date" Header="Ngày tạo" Width="100" Format="dd/MM/yyyy - HH:mm:ss"/>
            <ext:Column DataIndex="customer_code" Header="Mã KH" Width="100"/>
            <ext:Column DataIndex="customer_name" Header="Họ tên" Width="100"/>
            <ext:Column DataIndex="identity_number" Header="Số CMT/CCCD" Width="100"/>
            <ext:Column DataIndex="customer_type" Header="Loại hình" Width="100"/>
            <ext:Column DataIndex="request_status" Header="Trạng thái" Width="100">
                <Renderer Fn="renderRequestStatus"></Renderer>
            </ext:Column>
            <ext:Column DataIndex="created_by" Header="Người lập" Width="100"/>
            <ext:Column DataIndex="workflow_id" Header="Luồng xử lý" Width="100">
                <Renderer Fn="renderRequestWorkflow"></Renderer>
            </ext:Column>
            <%--<ext:Column DataIndex="rejection_reason" Header="Lý do từ chối (nếu có)" Width="100"/>--%>
        </Columns>
    </ColumnModel>
    <LoadMask ShowMask="true"></LoadMask>
    <SelectionModel>
        <ext:CheckboxSelectionModel runat="server" ID="chkSelectionModel"/>
    </SelectionModel>
    <BottomBar>
        <ext:PagingToolbar runat="server" PageSize="20" ID="pageTBar">
            <Items>
                <ext:Label runat="server" Text="Page size:"/>
                <ext:ComboBox runat="server" Width="80">
                    <Items>
                        <ext:ListItem Text="10"/>
                        <ext:ListItem Text="20"/>
                        <ext:ListItem Text="30"/>
                        <ext:ListItem Text="40"/>
                    </Items>
                    <SelectedItem Value="20"></SelectedItem>
                    <Listeners>
                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"></Select>
                    </Listeners>
                </ext:ComboBox>
            </Items>
        </ext:PagingToolbar>
    </BottomBar>
    <View>
        <ext:LockingGridView runat="server"/>
    </View>
    <Buttons>
        <ext:Button ID="btnUpdateStatusToCancellation" runat="server" Text="Hủy đăng ký" Icon="Cross">
            <Listeners>
                <Click Handler="if (!checkCancelledRow(#{chkSelectionModel})) {Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Lựa chọn tối đa 1 bản đăng ký cho mỗi lần Hủy', buttons:Ext.Msg.OK}); return false;}"></Click>
            </Listeners>
            <DirectEvents>
                <Click onEvent="btnUpdateStatusToCancellation_Click">
                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý chuyển sang trạng thái 'Đã hủy' không?"></Confirmation>
                    <EventMask ShowMask="true"></EventMask>
                    <ExtraParams>
                        <ext:Parameter Name="RequestData" Value="Ext.encode(#{grdRequestList}.getRowsValues({selectedOnly:true}))" Mode="Raw"/>
                    </ExtraParams>
                </Click>
            </DirectEvents>
        </ext:Button>
    </Buttons>
    <Listeners>
        <Command Handler="grdRequestListCommand(command, record.data);">
    </Command>
    <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
    </ViewReady>
    <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
    </Listeners>
</ext:GridPanel>
</Items>
</ext:Viewport>

<ext:Window ID="winUpdateRequest" runat="server" Title="Thông tin chi tiết đăng ký nhà đầu tư chuyên nghiệp" Hidden="true"
            Icon="Application" Layout="BorderLayout" ButtonAlign="Center" Closable="False"
            Padding="0" Modal="true">
<Items>
<ext:FormPanel Margins="0 0 5 0" runat="server" Region="North" ButtonAlign="Center" ID="frmSearchCustomer" Height="50">
    <Items>
        <ext:Container runat="server" Layout="Column" Height="50">
            <Items>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".1">
                    <Items>
                        <ext:Hidden runat="server" ID="hidCustomerSource"/>
                        <ext:Hidden runat="server" ID="hidCustomerCode"/>
                        <ext:Hidden runat="server" ID="hidBankCode"/>
                        <ext:Hidden runat="server" ID="hidBankCheqCode"/>
                        <ext:Hidden runat="server" ID="hidBranchCode"/>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:TextField ID="frmSearchCustomer_txtCustomerCode" runat="server" FieldLabel="Mã khách hàng" Width="200" LabelStyle="font-weight:bold;"
                                       MsgTarget="Side" AnchorHorizontal="96%"/>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:ComboBox runat="server" ID="frmSearchCustomer_cboCustomerSource" FieldLabel="Nguồn dữ liệu" Editable="false" SelectedIndex="0" Width="120" LabelStyle="font-weight:bold;" AnchorHorizontal="96%">
                            <Items>
                                <ext:ListItem Text="TBM" Value="10"/>
                                <ext:ListItem Text="BMS" Value="20"/>
                            </Items>
                            <SelectedItem Value="10"></SelectedItem>
                            <DirectEvents>
                                <Select onEvent="frmSearchCustomer_cboCustomerSource_Select"></Select>
                            </DirectEvents>
                        </ext:ComboBox>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".1">
                    <Items>
                        <ext:Button runat="server" Text="Tìm kiếm" ID="frmSearchCustomer_btnSearch" Icon="Find" CausesValidation="true">
                            <DirectEvents>
                                <Click OnEvent="frmSearchCustomer_btnSearch_Click"></Click>
                            </DirectEvents>
                        </ext:Button>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
                    <Items>
                        <ext:TextField ID="frmSearchCustomer_txtProType" runat="server" FieldLabel="Loại NĐT" Width="200" LabelStyle="font-weight:bold;"
                                       MsgTarget="Side" ReadOnly="True" AnchorHorizontal="96%"/>
                    </Items>
                </ext:Container>
                <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".1">
                    <Items>
                    </Items>
                </ext:Container>
            </Items>
        </ext:Container>
    </Items>
    <Buttons>
    </Buttons>
</ext:FormPanel>
<ext:FormPanel Margins="0 0 0 0" runat="server" Region="Center" ButtonAlign="Center" ID="frmUpdateRequest">
<Items>
<ext:Container runat="server" Layout="Column" Height="140">
    <Items>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtCustomerName" runat="server" FieldLabel="Họ tên" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtBirthday" runat="server" FieldLabel="Ngày sinh" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtIdentityNumber" runat="server" FieldLabel="Số CMT/CCCD" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtIssueDate" runat="server" FieldLabel="Ngày cấp" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtNationality" runat="server" FieldLabel="Quốc tịch" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:ComboBox runat="server" ID="frmUpdateRequest_cboRequestWorkflow" FieldLabel="Luồng xử lý" Editable="false" SelectedIndex="0" Width="200" LabelStyle="font-weight:bold;">
                    <Items>
                        <ext:ListItem Text="Vận hành trái phiếu (BMS)" Value="1"/>
                        <ext:ListItem Text="Kinh doanh trái phiếu (BB, BS)" Value="2"/>
                    </Items>
                    <SelectedItem Value="1"></SelectedItem>
                    <DirectEvents>
                        <Select onEvent="frmUpdateRequest_cboRequestWorkflow_Select"></Select>
                    </DirectEvents>
                </ext:ComboBox>
                <ext:ComboBox ID="frmUpdateRequest_cboTransactionFee" runat="server" EmptyText="Click xem chi tiết" TypeAhead="true" FieldLabel="Lịch sử Phí GD"
                              ForceSelection="False" Mode="Local" DisplayField="packagetype" ValueField="packagetype" Width="200" AllowBlank="True"
                              MinChars="1" ListWidth="320" PageSize="10" ItemSelector="tr.list-item" LabelStyle="font-weight:bold;">
                    <Store>
                        <ext:Store ID="storeTransactionFee" runat="server">
                            <Reader>
                                <ext:JsonReader>
                                    <Fields>
                                        <ext:RecordField Name="account"/>
                                        <ext:RecordField Name="custcode"/>
                                        <ext:RecordField Name="custacct"/>
                                        <ext:RecordField Name="effdate"/>
                                        <ext:RecordField Name="packagetype"/>
                                        <ext:RecordField Name="postdate"/>
                                        <ext:RecordField Name="delflag"/>
                                        <ext:RecordField Name="enddate"/>
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw"/>
                                <ext:Parameter Name="limit" Value="10" Mode="Raw"/>
                            </AutoLoadParams>
                        </ext:Store>
                    </Store>
                    <Template ID="Template1" runat="server">
                        <Html>
                        <tpl for=".">
                            <tpl if="[xindex] == 1">
                                <table class="cbStates-list">
                                <tr>
                                    <th style="font-weight: bold; text-align: center; width: 40px;">account</th>
                                    <th style="font-weight: bold; text-align: center;">packagetype</th>
                                    <th style="font-weight: bold; text-align: center;">effdate</th>
                                    <th style="font-weight: bold; text-align: center;">enddate</th>
                                </tr>
                            </tpl>
                            <tr class="list-item">
                                <td style="font-weight: bold; text-align: center;">{account}</td>
                                <td style="text-align: center;">{packagetype}</td>
                                <td style="text-align: center;">{effdate}</td>
                                <td style="text-align: center;">{enddate}</td>
                            </tr>
                            <tpl if="[xcount-xindex]==0">
                            </table>
                            </tpl>
                        </tpl>
                        </Html>
                    </Template>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true"/>
                    </Triggers>
                    <Listeners>
                        <BeforeQuery Handler="this.triggers[0][ this.getRawValue().toString().length == 0 ? 'hide' : 'show']();"/>
                        <TriggerClick Handler="if (index == 0) { this.focus().clearValue(); trigger.hide();}"/>
                        <Select Fn="cboTransactionFeeOnSelect"/>
                    </Listeners>
                </ext:ComboBox>
                <ext:TextField ID="frmUpdateRequest_txtIdentityNumber_Mirror" runat="server" FieldLabel="Số CMT/CCCD" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True" Visible="False"/>
                <ext:TextField ID="frmUpdateRequest_txtIssuePlace" runat="server" FieldLabel="Nơi cấp" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtCustomerType" runat="server" FieldLabel="Loại hình" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
    </Items>
</ext:Container>
<ext:Container runat="server" Layout="Column" Height="30">
    <Items>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".6">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtAddress" runat="server" FieldLabel="Địa chỉ" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="90%" ReadOnly="True"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
    </Items>
</ext:Container>
<ext:Container runat="server" Layout="Column" Height="125">
    <Items>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtAccountStatus" runat="server" FieldLabel="TT tài khoản" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:ComboBox ID="frmUpdateRequest_cboCustomerBank" runat="server" EmptyText="Click chọn ngân hàng" TypeAhead="true" FieldLabel="Ngân hàng đã đăng ký"
                              ForceSelection="False" Mode="Local" DisplayField="bank_account" ValueField="bank_account" Width="200" AllowBlank="True"
                              MinChars="1" ListWidth="320" PageSize="10" ItemSelector="tr.list-item" LabelStyle="font-weight:bold;">
                    <Store>
                        <ext:Store ID="storeCustomerBank" runat="server">
                            <Reader>
                                <ext:JsonReader>
                                    <Fields>
                                        <ext:RecordField Name="bank_account"/>
                                        <ext:RecordField Name="bank_beneficiary"/>
                                        <ext:RecordField Name="bank_code"/>
                                        <ext:RecordField Name="branch_code"/>
                                        <ext:RecordField Name="bank_name"/>
                                        <ext:RecordField Name="bank_cheq_code"/>
                                        <ext:RecordField Name="branch_name"/>
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw"/>
                                <ext:Parameter Name="limit" Value="10" Mode="Raw"/>
                            </AutoLoadParams>
                        </ext:Store>
                    </Store>
                    <Template ID="Template2" runat="server">
                        <Html>
                        <tpl for=".">
                            <tpl if="[xindex] == 1">
                                <table class="cbStates-list">
                                <tr>
                                    <th style="font-weight: bold; text-align: center; width: 40px;">STK</th>
                                    <th style="font-weight: bold; text-align: center;">Ngân hàng</th>
                                    <th style="font-weight: bold; text-align: center;">Chi nhánh</th>
                                    <%--<th style="font-weight: bold; text-align: center;">bank_cheq_code</th>--%>
                                </tr>
                            </tpl>
                            <tr class="list-item">
                                <td style="font-weight: bold; text-align: center;">{bank_account}</td>
                                <td style="text-align: center;">{bank_name}</td>
                                <td style="text-align: center;">{branch_name}</td>
                                <%--<td style="text-align: center;">{bank_cheq_code}</td>--%>
                            </tr>
                            <tpl if="[xcount-xindex]==0">
                            </table>
                            </tpl>
                        </tpl>
                        </Html>
                    </Template>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true"/>
                    </Triggers>
                    <Listeners>
                        <BeforeQuery Handler="this.triggers[0][ this.getRawValue().toString().length == 0 ? 'hide' : 'show']();"/>
                        <TriggerClick Handler="if (index == 0) { this.focus().clearValue(); trigger.hide(); #{hidBankCode}.clear(); #{hidBankCheqCode}.clear(); #{hidBranchCode}.clear(); #{frmUpdateRequest_txtBankAccount}.clear(); #{frmUpdateRequest_txtBankBeneficiary}.clear(); #{frmUpdateRequest_txtBankName}.clear(); #{frmUpdateRequest_txtBranchName}.clear();}"/>
                        <Select Fn="cboCustomerBankOnSelect"/>
                    </Listeners>
                </ext:ComboBox>
                <ext:ComboBox ID="frmUpdateRequest_cboBankList" runat="server" EmptyText="Click chọn ngân hàng" TypeAhead="true" FieldLabel="Ngân hàng tùy chọn"
                              ForceSelection="False" Mode="Local" DisplayField="bank_name" ValueField="bank_name" Width="200" AllowBlank="True"
                              MinChars="1" ListWidth="320" PageSize="10" ItemSelector="tr.list-item" LabelStyle="font-weight:bold;" Hidden="True">
                    <Store>
                        <ext:Store ID="storeBankList" runat="server">
                            <Reader>
                                <ext:JsonReader>
                                    <Fields>
                                        <ext:RecordField Name="bank_code"/>
                                        <ext:RecordField Name="branch_code"/>
                                        <ext:RecordField Name="bank_name"/>
                                        <ext:RecordField Name="bank_cheq_code"/>
                                        <ext:RecordField Name="branch_name"/>
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                            <AutoLoadParams>
                                <ext:Parameter Name="start" Value="0" Mode="Raw"/>
                                <ext:Parameter Name="limit" Value="10" Mode="Raw"/>
                            </AutoLoadParams>
                        </ext:Store>
                    </Store>
                    <Template ID="Template3" runat="server">
                        <Html>
                        <tpl for=".">
                            <tpl if="[xindex] == 1">
                                <table class="cbStates-list">
                                <tr>
                                    <th style="font-weight: bold; text-align: center; width: 40px;">Ngân hàng</th>
                                    <th style="font-weight: bold; text-align: center;">Chi nhánh</th>
                                    <th style="font-weight: bold; text-align: center;">bank_cheq_code</th>
                                </tr>
                            </tpl>
                            <tr class="list-item">
                                <td style="text-align: center;">{bank_name}</td>
                                <td style="text-align: center;">{branch_name}</td>
                                <td style="text-align: center;">{bank_cheq_code}</td>
                            </tr>
                            <tpl if="[xcount-xindex]==0">
                            </table>
                            </tpl>
                        </tpl>
                        </Html>
                    </Template>
                    <Triggers>
                        <ext:FieldTrigger Icon="Clear" HideTrigger="true"/>
                    </Triggers>
                    <Listeners>
                        <BeforeQuery Handler="this.triggers[0][ this.getRawValue().toString().length == 0 ? 'hide' : 'show']();"/>
                        <TriggerClick Handler="if (index == 0) { this.focus().clearValue(); trigger.hide(); #{hidBankCode}.clear(); #{hidBankCheqCode}.clear(); #{hidBranchCode}.clear(); #{frmUpdateRequest_txtBankAccount}.clear(); #{frmUpdateRequest_txtBankBeneficiary}.clear(); #{frmUpdateRequest_txtBankName}.clear(); #{frmUpdateRequest_txtBranchName}.clear();}"/>
                        <Select Fn="cboBankListOnSelect"/>
                    </Listeners>
                </ext:ComboBox>
                <ext:TextField ID="frmUpdateRequest_txtBankAccount" runat="server" FieldLabel="STK" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtBankName" runat="server" FieldLabel="Ngân hàng" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtDocumentStatus" runat="server" FieldLabel="TT hồ sơ" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:RadioGroup ID="frmUpdateRequest_rbgBankSourceSelection" runat="server" Vertical="True" ColumnsNumber="1">
                    <Items>
                        <ext:Radio ID="frmUpdateRequest_rbRegisteredBank" runat="server" BoxLabel="Ngân hàng đã đăng ký" Checked="True">
                            <DirectEvents>
                                <Check onEvent="frmUpdateRequest_rbRegisteredBank_Check">
                                </Check>
                            </DirectEvents>
                        </ext:Radio>
                        <ext:Radio ID="frmUpdateRequest_rbCustomBank" runat="server" BoxLabel="Ngân hàng tùy chọn">
                            <DirectEvents>
                                <Check onEvent="frmUpdateRequest_rbCustomBank_Check">
                                </Check>
                            </DirectEvents>
                        </ext:Radio>
                    </Items>
                </ext:RadioGroup>
                <ext:TextField ID="frmUpdateRequest_txtBankBeneficiary" runat="server" FieldLabel="Chủ tài khoản" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
                <ext:TextField ID="frmUpdateRequest_txtBranchName" runat="server" FieldLabel="Chi nhánh" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="80%" ReadOnly="True"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
    </Items>
</ext:Container>
<ext:Container runat="server" Layout="Column" Height="60">
    <Items>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".6">
            <Items>
                <ext:TextField ID="frmUpdateRequest_txtNote" runat="server" FieldLabel="Ghi chú" Width="200" LabelStyle="font-weight:bold;"
                               MsgTarget="Side" AnchorHorizontal="90%"/>
                <ext:Label ID="frmUpdateRequest_lblPreviousRecord" runat="server" FieldLabel="" Width="200" LabelStyle="font-weight:bold;"
                           StyleSpec="font-weight:bold; color:red;" MsgTarget="Side" AnchorHorizontal="90%"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
    </Items>
</ext:Container>
<ext:Container runat="server" Layout="Column" Height="50">
    <Items>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:Label ID="frmUpdateRequest_lblCreatedBy" runat="server" FieldLabel="Người tạo" Width="200" LabelStyle="font-weight:bold;"
                           AnchorHorizontal="90%"/>
                <ext:Label ID="frmUpdateRequest_lblUpdatedBy" runat="server" FieldLabel="Người cập nhật" Width="200" LabelStyle="font-weight:bold;"
                           AnchorHorizontal="90%"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".3">
            <Items>
                <ext:Label ID="frmUpdateRequest_lblCreatedDate" runat="server" FieldLabel="Ngày tạo" Width="200" LabelStyle="font-weight:bold;"
                           AnchorHorizontal="90%"/>
                <ext:Label ID="frmUpdateRequest_lblUpdatedDate" runat="server" FieldLabel="Ngày cập nhật" Width="200" LabelStyle="font-weight:bold;"
                           AnchorHorizontal="90%"/>
            </Items>
        </ext:Container>
        <ext:Container runat="server" LabelAlign="Left" Layout="Form" ColumnWidth=".2">
            <Items>
            </Items>
        </ext:Container>
    </Items>
</ext:Container>
</Items>
<Buttons>
</Buttons>
</ext:FormPanel>
</Items>
<Buttons>
    <ext:Button ID="frmUpdateRequest_btnClose" runat="server" Text="Đóng">
        <Listeners>
            <Click Handler="#{frmSearchCustomer}.reset(); #{frmUpdateRequest}.reset(); #{winUpdateRequest}.hide();"></Click>
        </Listeners>
    </ext:Button>
    <ext:Button ID="frmUpdateRequest_btnUpdate" runat="server" Text="Cập nhật" Icon="ApplicationGo">
        <DirectEvents>
            <Click onEvent="frmUpdateRequest_btnUpdate_Click">
                <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật không?"></Confirmation>
            </Click>
        </DirectEvents>
    </ext:Button>
</Buttons>
</ext:Window>

<ext:Window ID="winViewActionLog" runat="server" Title="Action Log" Hidden="true"
            Icon="Application" Layout="BorderLayout" ButtonAlign="Center" Closable="False"
            Padding="0" Modal="true" Width="720" Height="480">
    <Items>
        <ext:FormPanel Margins="0 0 5 0" runat="server" Region="North" ButtonAlign="Center" ID="frmSearchActionLog" Height="50">
            <Items>
            </Items>
        </ext:FormPanel>
        <ext:GridPanel ID="grdActionLogList" runat="server" Margins="0 0 0 0" Region="Center"
                       ColumnLines="True" StripeRows="True" ButtonAlign="Center">
            <Store>
                <ext:Store ID="storeActionLogList" runat="server" OnRefreshData="storeActionLogList_Refresh">
                    <Reader>
                        <ext:JsonReader IDProperty="action_log_id">
                            <Fields>
                                <ext:RecordField Name="action_log_id"/>
                                <ext:RecordField Name="action_id"/>
                                <ext:RecordField Name="action_name"/>
                                <ext:RecordField Name="action_body"/>
                                <ext:RecordField Name="request_id"/>
                                <ext:RecordField Name="created_date"/>
                                <ext:RecordField Name="created_by"/>
                            </Fields>
                        </ext:JsonReader>
                    </Reader>
                </ext:Store>
            </Store>
            <ColumnModel runat="server">
                <Columns>
                    <ext:Column Header="STT" Fixed="true" Sortable="false" ColumnID="stt" MenuDisabled="true" Width="40">
                        <Renderer Fn="function (v, p, record, rowIndex) {return rowIndex + 1}"></Renderer>
                    </ext:Column>
                    <ext:Column DataIndex="action_log_id" Header="Mã định danh" Width="100"/>
                    <ext:DateColumn DataIndex="created_date" Header="Ngày tạo" Width="100" Format="dd/MM/yyyy - HH:mm:ss"/>
                    <ext:Column DataIndex="created_by" Header="Người tạo" Width="100"/>
                    <ext:Column DataIndex="action_name" Header="Thao tác" Width="100"/>
                    <ext:Column DataIndex="action_body" Header="Nội dung" Width="100"/>
                    <ext:Column DataIndex="request_id" Header="Request" Width="100"/>
                </Columns>
            </ColumnModel>
            <LoadMask ShowMask="true"></LoadMask>
            <BottomBar>
                <ext:PagingToolbar runat="server" PageSize="20" ID="pageTBarActionLog">
                    <Items>
                        <ext:Label runat="server" Text="Page size:"/>
                        <ext:ComboBox runat="server" Width="80">
                            <Items>
                                <ext:ListItem Text="10"/>
                                <ext:ListItem Text="20"/>
                                <ext:ListItem Text="30"/>
                                <ext:ListItem Text="40"/>
                            </Items>
                            <SelectedItem Value="20"></SelectedItem>
                            <Listeners>
                                <Select Handler="#{pageTBarActionLog}.pageSize = parseInt(this.getValue()); #{pageTBarActionLog}.doLoad();"></Select>
                            </Listeners>
                        </ext:ComboBox>
                    </Items>
                </ext:PagingToolbar>
            </BottomBar>
            <View>
                <ext:LockingGridView runat="server"/>
            </View>
            <Buttons>
                <ext:Button ID="winViewActionLog_btnClose" runat="server" Text="Đóng">
                    <Listeners>
                        <Click Handler="#{winViewActionLog}.hide();"></Click>
                    </Listeners>
                </ext:Button>
            </Buttons>
            <Listeners>
                <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
                </ViewReady>
                <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
            </Listeners>
        </ext:GridPanel>
    </Items>
</ext:Window>
</form>
</body>
</html>
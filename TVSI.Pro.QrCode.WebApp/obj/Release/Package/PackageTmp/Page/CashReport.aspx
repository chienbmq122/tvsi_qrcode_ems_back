﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CashReport.aspx.cs" Inherits="TVSI.Pro.Investor.WebApp.Page.CashReport" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner { font-weight: bold !important; }
    </style>
    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function logout() {
                window.top.location.href = 'http://ems.tvsi.com.vn/sys/tvsi-login/default.aspx';
            }
            
            var columnAutoResize = function (grid) {
                var view = grid.getView(),
                    store = grid.getStore(),
                    colModel = grid.getColumnModel(),
                    columns = colModel.config,
                    maxAutoWidth = 250, //0 to disable
                    cell,
                    value,
                    width = 0;
            
                Ext.each(columns, function (column, colIdx) {
                    // Data Width
                    var colWidth = width;
                    store.each(function (record, rowIdx) {
                        cell = view.getCell(rowIdx, colIdx);
                        value = record.get(column.dataIndex);
                        colWidth = Math.max(colWidth, Ext.util.TextMetrics.measure(cell, value).width);
                    });
            
                    if (!column.isRowNumberer) {
                        // Header Width
                        header = view.getHeaderCell(colIdx);
                        headerWidth = Ext.util.TextMetrics.measure(header, column.header).width;
                    }
                    else {
                        // Calc width using total rows
                        lengthWidth = store.getTotalCount().toString().length;
                        headerWidth = (lengthWidth * 10) + (lengthWidth > 1 ? 0 : 10);
                    }
            
                    // Choose the biggest width
                    if (colWidth < headerWidth || colWidth == 0) {
                        colWidth = headerWidth;
                    }
            
                    // Max Length
                    if (colWidth > maxAutoWidth && maxAutoWidth > 0) {
                        colWidth = maxAutoWidth;
                    }
            
                    //Add space to avoid ...
                    if (!column.isRowNumberer) {
                        colWidth += 20
                    }
            
                    colModel.setColumnWidth(colIdx, colWidth);
                });
            };
        </script>
    </ext:XScript>
    <script type="text/javascript">
        function IsNullOrWhiteSpace(str){
            return str === null || str.match(/^\s*$/) !== null;
        }
    </script>
</head>
<body>
<form id="form1" runat="server">
    <ext:ResourceManager runat="server"/>
    <ext:Viewport runat="server" layout="BorderLayout">
        <Items>
            <ext:FormPanel Margins="0 0 0 0" runat="server" Region="North" ButtonAlign="Center" ID="frmSearchRequest" Height="100">
                <Items>
                    <ext:Container runat="server" Layout="Column" Height="100">
                        <Items>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".1">
                                <Items>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                                <Items>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                                <Items>
                                    <ext:TextField ID="frmSearchRequest_txtCustomerCode" runat="server" FieldLabel="Số tài khoản" Width="200" LabelStyle="font-weight:bold;"
                                                   MsgTarget="Side" AnchorHorizontal="50%"/>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                                <Items>
                                    <ext:DateField runat="server" ID="frmSearchRequest_dtxFromDate" FieldLabel="Từ ngày" Format="dd/MM/yyyy" MaskRe="[0-9\/]" LabelStyle="font-weight:bold;" AnchorHorizontal="50%">
                                        <DirectEvents>
                                            <Change onEvent="frmSearchRequest_dtxFromDate_Change"></Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".2">
                                <Items>
                                    <ext:DateField runat="server" ID="frmSearchRequest_dtxToDate" FieldLabel="Đến ngày" Format="dd/MM/yyyy" MaskRe="[0-9\/]" LabelStyle="font-weight:bold;" AnchorHorizontal="50%">
                                        <DirectEvents>
                                            <Change onEvent="frmSearchRequest_dtxToDate_Change"></Change>
                                        </DirectEvents>
                                    </ext:DateField>
                                </Items>
                            </ext:Container>
                            <ext:Container runat="server" LabelAlign="Top" Layout="Form" ColumnWidth=".1">
                                <Items>

                                </Items>
                            </ext:Container>
                        </Items>
                    </ext:Container>
                </Items>
                <Buttons>
                    <ext:Button runat="server" Text="Tìm kiếm" ID="frmSearchRequest_btnSearch" Icon="Find" CausesValidation="true">
                        <DirectEvents>
                            <Click OnEvent="frmSearchRequest_btnSearch_Click"></Click>
                        </DirectEvents>
                        <Listeners>
                            <Click Handler="#{storeRequestList}.reload()"></Click>
                        </Listeners>
                    </ext:Button>
                    <ext:Button runat="server" Text="Reset" ID="frmSearchRequest_btnReset" Icon="Stop">
                        <Listeners>
                            <Click Handler="#{frmSearchRequest}.reset()"></Click>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="frmSearchRequest_btnReset_Click"></Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button runat="server" Text="Kết xuất Excel" ID="frmSearchRequest_btnExportExcel" AutoPostBack="true" Icon="PageExcel" OnClick="frmSearchRequest_btnExportExcel_Click">
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
            <ext:GridPanel ID="grdRequestList" runat="server" Margins="0 0 0 0" Region="Center"
                           ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                <Store>
                    <ext:Store ID="storeRequestList" runat="server" OnRefreshData="storeRequestList_Refresh">
                        <Reader>
                            <ext:JsonReader>
                                <Fields>
                                    <ext:RecordField Name="cash_amount"/>
                                    <ext:RecordField Name="customer_code"/>
                                    <ext:RecordField Name="created_date"/>
                                </Fields>
                            </ext:JsonReader>
                        </Reader>
                    </ext:Store>
                </Store>
                <ColumnModel runat="server">
                    <Columns>
                        <ext:Column Header="STT" Fixed="true" Sortable="false" ColumnID="stt" MenuDisabled="true" Width="40">
                            <Renderer Fn="function (v, p, record, rowIndex) {return rowIndex + 1}"></Renderer>
                        </ext:Column>
                        <ext:DateColumn DataIndex="created_date" Header="Ngày" Width="100" Format="dd/MM/yyyy"/>
                        <ext:Column DataIndex="customer_code" Header="Số TK" Width="100"/>
                        <ext:NumberColumn DataIndex="cash_amount" Header="Số tiền" Width="100" Format="00,000"/>
                    </Columns>
                </ColumnModel>
                <LoadMask ShowMask="true"></LoadMask>
                <BottomBar>
                    <ext:PagingToolbar runat="server" PageSize="20" ID="pageTBar">
                        <Items>
                            <ext:Label runat="server" Text="Page size:"/>
                            <ext:ComboBox runat="server" Width="80">
                                <Items>
                                    <ext:ListItem Text="10"/>
                                    <ext:ListItem Text="20"/>
                                    <ext:ListItem Text="30"/>
                                    <ext:ListItem Text="40"/>
                                </Items>
                                <SelectedItem Value="20"></SelectedItem>
                                <Listeners>
                                    <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();"></Select>
                                </Listeners>
                            </ext:ComboBox>
                        </Items>
                    </ext:PagingToolbar>
                </BottomBar>
                <View>
                    <ext:LockingGridView runat="server"/>
                </View>
                <Buttons>
                </Buttons>
                <Listeners>
                    <ViewReady Handler="columnAutoResize(this); this.getStore().on('load', Ext.createDelegate(columnAutoResize, null, [this]));" Delay="10">
                    </ViewReady>
                    <HeaderDblClick Fn="columnAutoResize"></HeaderDblClick>
                </Listeners>
            </ext:GridPanel>
        </Items>
    </ext:Viewport>

</form>
</body>
</html>
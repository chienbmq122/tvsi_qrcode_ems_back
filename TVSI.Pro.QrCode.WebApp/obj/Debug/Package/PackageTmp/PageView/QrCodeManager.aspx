﻿<%@ Page Language="C#" CodeBehind="QrCodeManager.aspx.cs" Inherits="TVSI.Pro.QrCode.WebApp.PageView.QrCodeManager" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript">
    </script>
    <title></title>
    <style type="text/css">
        .x-grid3-hd-inner {
            font-weight: bold !important;
        }
    </style>

    <ext:XScript ID="XScript1" runat="server">
        <script type="text/javascript">
            function grdStockApiList(command, data) {          
                if (command == "View") {     
                    USERMETHOD.ViewUserQRDetail(data.QRCodeID);
                }   
                if (command == "Edit") {     
                    USERMETHOD.ViewServiceQRDetail(data.QRCodeID);
                }
                if (command == "Delete") {
                    USERMETHOD.DeleteUserQRInfo(data.QRCodeID);
                }
            }
            var template = '<span style="background-color:{0};text-align:center;border: 1px solid #ccc;line-height: 20px;">{1}</span>';

  		         var renderSPStatus = function (value) {
                  if (value == "-1")
                      return String.format(template, "#ffab43", "Từ chối");
                  if (value == "0")
                      return String.format(template, "#1ae5e5", "Tạo mới");
                  if (value == "1")
                      return String.format(template, "#1cff00", " Thành công");
                  if(value == "5")
                       return String.format(template, "#f108e5", "Hủy đăng ký gói"); 
                  if (value == "99")
                      return String.format(template, "#de081a", "Xóa");  
                  if (value == "-2")
                      return String.format(template, "#134bf5", "Đã hủy");   
                  if(value == "-3")
                      return String.format(template, "#721101", "Ngừng sử dụng");
                     
  					return value;
              };
            var preStatus = function (grid, toolbar, rowIndex, record) {
                                    if (record.data.trang_thai != 0) {
                                        toolbar.items.itemAt(0).hide();
             }
            };
        </script>
    </ext:XScript>
</head>
<body>

    <ext:ResourceManager runat="server" />
    <form id="form2" runat="server">
        <ext:Viewport ID="Viewport1" runat="server" Layout="BorderLayout">
            <Items>
                <ext:Panel ID="Panel1" Margins="0 0 0 5" runat="server" Region="North">
                    <TopBar>
                        <ext:Toolbar runat="server">
                            <Items>
                                <ext:Label Text="SaleID" runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchSaleID" Width="80" />
                                <ext:ToolbarSeparator />
                                <ext:Label Text="Mã chi nhánh" runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:ComboBox runat="server" ID="cboSearchBranchID" Width="100">
                                    <Listeners>
                                        <Select Handler="#{Refresh_Data}.reload()"></Select>
                                    </Listeners>
                                </ext:ComboBox>
                                <ext:ToolbarSpacer/>
                                <ext:ToolbarSeparator/>
                                <ext:Label Text="Họ tên" runat="server" />
                                <ext:ToolbarSpacer />
                                <ext:TextField runat="server" ID="txtSearchFullName" Width="80" />
                                <ext:ToolbarSeparator/>
                                <ext:Label Text="Số điện thoại" runat="server"/>
                                <ext:ToolbarSpacer/>
                                <ext:TextField runat="server" ID="txtSearchPhone" Width="80"/>
                                <ext:ToolbarSeparator/>
                                <ext:Button runat="server" Text="Tìm kiếm" ID="btnSearch" Icon="find" CausesValidation="true">
                                    <Listeners>
                                        <Click Handler="#{storeQRListService}.reload()" />
                                    </Listeners>
                                </ext:Button>
                                <ext:ToolbarSeparator />
                                <ext:Button runat="server" Text="Thêm mới" ID="btnAdd7number" Icon="ApplicationAdd" CausesValidation="true">
                                    <DirectEvents>
                                        <Click OnEvent="btnAddUser7number_Click" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Toolbar>
                    </TopBar>
                </ext:Panel>
                <ext:GridPanel ID="grdQRListService" runat="server" Margins="0 0 0 5" Region="Center"
                    ColumnLines="True" StripeRows="True" ButtonAlign="Center">
                    <Store>
                        <ext:Store ID="storeQRListService" runat="server" OnRefreshData="Refresh_Data">
                            <Reader>
                                <ext:JsonReader IDProperty="QRCodeID">
                                    <Fields>
                                        <ext:RecordField Name="QRCodeID" />
                                        <ext:RecordField Name="BranchID" />
                                        <ext:RecordField Name="SaleID" />
                                        <ext:RecordField Name="FullName" />
                                        <ext:RecordField Name="Phone" />
                                        <ext:RecordField Name="LinkZalo" />
                                        <ext:RecordField Name="CreatedBy" />
                                        <ext:RecordField Name="CreatedDate" />
                                        <ext:RecordField Name="UpdatedBy" />
                                        <ext:RecordField Name="UpdatedDate" />
                                    </Fields>
                                </ext:JsonReader>
                            </Reader>
                        </ext:Store>
                    </Store>
                    <ColumnModel ID="ColumnModel1" runat="server">
                        <Columns>
                            <ext:RowNumbererColumn Width="20" Header="#" />
                            <ext:CommandColumn Width="60" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Zoom" CommandName="View" Text="View"/>
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="60" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="ApplicationEdit" CommandName="Edit" Text="Edit"/>
                                </Commands>
                            </ext:CommandColumn>
                            <ext:CommandColumn Width="60" DataIndex="command">
                                <Commands>
                                    <ext:GridCommand Icon="Delete" CommandName="Delete" Text="Delete"/>
                                </Commands>
                            </ext:CommandColumn>
                            <ext:Column DataIndex="QRCodeID" Header="QR ID" Width="60" />
                            <ext:Column DataIndex="BranchID" Header="Mã chi nhánh" Width="100" />
                            <ext:Column DataIndex="SaleID" Header="SaleID" Width="120" />
                            <ext:Column DataIndex="FullName" Header="Họ và tên" Width="150" />
                            <ext:Column DataIndex="Phone" Header="Số điện thoại" Width="150" />
                            <ext:Column DataIndex="LinkZalo" Header="Link Zalo" Width="150" />
                            <ext:Column DataIndex="CreatedBy" Header="Người tạo" Width="150"/>
                            <ext:DateColumn DataIndex="CreatedDate" Header="Ngày tạo" Width="150"  Format="dd/MM/yyyy HH:mm:ss"/>
                            <ext:Column DataIndex="UpdatedBy" Header="Người cập nhật cuối cùng" Width="150"/>
                            <ext:DateColumn DataIndex="UpdatedDate" Header="Ngày cập nhật cuối cùng" Width="150" Format="dd/MM/yyyy HH:mm:ss"/>
                        </Columns>
                    </ColumnModel>
                    <Listeners>
                        <Command Handler="grdStockApiList(command, record.data);"></Command>
                    </Listeners>
                    <LoadMask ShowMask="true" />
                    <BottomBar>
                        <ext:PagingToolbar runat="server" PageSize="40" ID="pageTBar">
                            <Items>
                                <ext:Label ID="Label1" runat="server" Text="Page size:" />
                                <ext:ComboBox ID="ComboBox1" runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Text="20" />
                                        <ext:ListItem Text="40" />
                                        <ext:ListItem Text="60" />
                                        <ext:ListItem Text="80" />
                                    </Items>
                                    <SelectedItem Value="40" />
                                    <Listeners>
                                        <Select Handler="#{pageTBar}.pageSize = parseInt(this.getValue()); #{pageTBar}.doLoad();" />
                                    </Listeners>
                                </ext:ComboBox>
                            </Items>
                        </ext:PagingToolbar>
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true" />
                    </SelectionModel>
                </ext:GridPanel>
            </Items>
    </ext:Viewport>
    <ext:Window runat="server" ID="winServiceZaloDetail" Title="Chỉnh sửa QR Code Zalo" Hidden="True"
                Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
                BodyStyle="background-color: #fff;" ButtonAlign="Center">
        <Items>
            <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditService">
                <Items>
                    <ext:Hidden runat="server" ID="editID"></ext:Hidden>
                    <ext:TextField runat="server" ID="txtEditSaleIDtxt" Width="200" FieldLabel="SaleID" AllowBlank="False" />
                    <ext:ComboBox runat="server" ID="cboEditBranchIDtxt" Width="200" FieldLabel="Mã chi nhánh" AllowBlank="False"/>
                    <ext:TextField runat="server" ID="txtEditFullNametxt" Width="200" FieldLabel="Họ và tên" AllowBlank="False"/>
                    <ext:TextField runat="server" ID="txtEditPhonetxt" Width="200" FieldLabel="Số điện thoại" AllowBlank="False"/>
                    <ext:TextField runat="server" ID="txtEditLinkZalotxt" Width="200" FieldLabel="Link Zalo" AllowBlank="False" ReadOnly="True"/>
                    <ext:Image runat="server" ID="txtEditImagetxt" ImageUrl="" Width="150px" Height="150px"/>
                    <ext:FileUploadField ID="txtEditUploadQRZalo" runat="server" Width="225" FieldLabel="ảnh QR Zalo" Icon="Attach" />

                </Items>
                <Buttons>
                    <ext:Button runat="server" Text="Cập nhật" Icon="ApplicationEdit">
                        <Listeners>
                            <Click Handler="if(#{frmEditService}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}"/>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="EditQRZaloInfo">
                                <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý cập nhật thông tin này?"/>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>


        <ext:Window runat="server" ID="winUserQRDetail" Title="Thông tin QR Zalo" Hidden="True"
            Icon="Application" Layout="FitLayout" Height="400" Width="600" Padding="5" Modal="True"
            BodyStyle="background-color: #fff;" ButtonAlign="Center">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmEditUser">
                    <Items>
                        <ext:Hidden runat="server" ID="hidID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtEditSaleID" Width="200" FieldLabel="SaleID" AllowBlank="False" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditBranchID" Width="200" FieldLabel="Mã chi nhánh" AllowBlank="False" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditFullName" Width="200" FieldLabel="Họ và tên" AllowBlank="False" ReadOnly="True" />
                        <ext:TextField runat="server" ID="txtEditPhone" Width="200" FieldLabel="Số điện thoại" ReadOnly="True"/>
                        <ext:Image runat="server" ID="txtEditImage" ImageUrl=""  Width="150px" Height="150px"/>
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Đóng" Icon="Accept">
                            <DirectEvents>
                                <Click OnEvent="btnViewUser_Click" />
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>

        <ext:Window runat="server" ID="winDeleteQRCode" Title="Xóa thông tin QR Code"
            Hidden="True" Icon="Application" Height="450" Width="450" Layout="FitLayout" Modal="True">
            <Items>
                <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="formDelQR">
                    <Items>
                        <ext:Hidden runat="server" ID="hidDelID"></ext:Hidden>
                        <ext:TextField runat="server" ID="txtDelSaleID" Width="200" FieldLabel="SaleID" AllowBlank="False" ReadOnly="True"/>
                        <ext:TextField runat="server" ID="txtDelBranchID" Width="200" FieldLabel="Mã chi nhánh" AllowBlank="False" ReadOnly="True"/>
                        <ext:TextField runat="server" ID="txtDelFullName" Width="200" FieldLabel="Họ và tên" AllowBlank="False" ReadOnly="True"/>
                        <ext:TextField runat="server" ID="txtDelPhone" Width="200" FieldLabel="Số điện thoại" ReadOnly="True"/>
                        <ext:Image runat="server" ID="txtDelImage" ImageUrl="" Width="150px" Height="150px"/>
                    </Items>
                    <Buttons>
                        <ext:Button runat="server" Text="Xóa yêu cầu" Icon="Delete">
                            <DirectEvents>
                                <Click OnEvent="btnDeleteUser_Click">
                                    <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý với việc xóa yêu này?" />
                                </Click>
                            </DirectEvents>
                        </ext:Button>
                    </Buttons>
                </ext:FormPanel>
            </Items>
        </ext:Window>
    <ext:Window runat="server" ID="winPopupAdd" Title="Thêm thông tin QR Zalo" Hidden="True" Icon="Application" Height="500" Width="500" Layout="FitLayout" Modal="True">
        <Items>
            <ext:FormPanel runat="server" Border="False" LabelWidth="150" Padding="10" ButtonAlign="Center" ID="frmAddUser7number">
                <Items>

                    <ext:TextField runat="server" ID="txtAddSaleID" Width="150" FieldLabel="điền SaleID" AllowBlank="False" />
                    <ext:Button runat="server" Height="10" Width="10" ID="txtButtonGetSaleInfo" Text="Lấy thông tin" Icon="ApplicationGet">
                        <DirectEvents>
                            <Click OnEvent="GetSaleInfo">
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:TextField runat="server" ID="txtAddBranchID" Width="150" FieldLabel="Mã chi nhánh" AllowBlank="False" ReadOnly="True"/>
                    <ext:TextField runat="server" ID="txtAddFullName" Width="150" FieldLabel="Họ và tên" AllowBlank="False" ReadOnly="True"/>
                    <ext:TextField runat="server" ID="txtAddPhone" Width="150" FieldLabel="Số điện thoại" AllowBlank="False"/>
                    <ext:FileUploadField ID="UploadQRZaloFiled" runat="server" Width="225" FieldLabel="Thêm ảnh QR Zalo" Icon="Attach" AllowBlank="False"/>

                </Items>
                <Buttons>
                    <ext:Button runat="server" ID="btnConfAddUser7number" Text="Thêm mới" Icon="ApplicationAdd" Width="100">
                        <Listeners>
                            <Click Handler="if(#{frmAddUser7number}.getForm().isValid()) { return true; }else{Ext.Msg.show({icon: Ext.MessageBox.ERROR, msg: 'Bạn chưa nhập đầy đủ và đúng thông tin', buttons:Ext.Msg.OK}); return false;}"/>
                        </Listeners>
                        <DirectEvents>
                            <Click OnEvent="AddProfileQRZalo">
                                <Confirmation ConfirmRequest="true" Title="Xác nhận" Message="Bạn có đồng ý thêm thông tin này?"/>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Window>
    <ext:KeyMap ID="KeyMap1" runat="server" Target="#{txtSearchAccountNo}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
        <ext:KeyMap ID="KeyMap2" runat="server" Target="#{txtSearchUserName}">
            <ext:KeyBinding StopEvent="true">
                <Keys>
                    <ext:Key Code="ENTER" />
                </Keys>
                <Listeners>
                    <Event Handler="#{storeListUserService}.reload()" />
                </Listeners>
            </ext:KeyBinding>
        </ext:KeyMap>
    </form>
</body>
</html>
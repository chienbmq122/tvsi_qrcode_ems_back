﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Menu.aspx.cs" Inherits="TVSI.Pro.Investor.WebApp.Menu" %>
<%@ Register TagPrefix="ext" Namespace="Ext.Net" Assembly="Ext.Net, Version=1.2.0.21945, Culture=neutral, PublicKeyToken=2e12ce3d0176cd87" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<ext:ResourceManager runat="server"/>
<form id="form1" runat="server">
    <ext:Viewport ID="Viewport1" runat="server">
        <Items>
            <ext:BorderLayout runat="server">
                <West>
                    <ext:TreePanel ID="treePanelMenu" runat="server" RootVisible="False"
                                   Width="300" Title="EMS" Icon="ChartOrganisation">
                        <Listeners>
                            <Click Handler="if (node.attributes.href) { e.stopEvent(); loadPage(#{mainContent}, node); }"/>
                        </Listeners>
                    </ext:TreePanel>
                </West>
                <Center>
                    <ext:TabPanel ID="mainContent" runat="server" EnableTabScroll="true"/>
                </Center>
            </ext:BorderLayout>
        </Items>
    </ext:Viewport>
</form>
<ext:XScript ID="XScript1" runat="server">
    <script type="text/javascript">
            var loadPage = function (tabPanel, node) {
                var tab = tabPanel.getItem(node.id);

                if (!tab) {
                    tab = tabPanel.add({
                        id: node.id,
                        title: node.text,
                        closable: true,
                        autoLoad: {
                            showMask: true,
                            url: node.attributes.href,
                            mode: "iframe",
                            maskMsg: "Loading " + node.attributes.href + "..."
                        },
                        listeners: {
                            update: {
                                fn: function (tab, cfg) {
                                    cfg.iframe.setHeight(cfg.iframe.getSize().height - 20);
                                },
                                scope: this,
                                single: true
                            }
                        }
                    });
                }

                tabPanel.setActiveTab(tab);
            }
            </script>
</ext:XScript>
</body>
</html>
